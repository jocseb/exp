---
title: "Tuto : utilisation de Mkdocs"
author: []
date: "29 août 2022"
subject: "Tuto"
keywords: []
lang: "fr"
header-left: Spé info
header-center: Tuto
header-right:
footer-left:
footer-center: "Thème: "
footer-right: "Page \\thepage"
...

# Divers tutoriels, aide-mémoire, fourre-tout, notes pour prochains cours ...

## Mkdocs
MkDocs est un projet alimenté par Python utilisé pour créer de la documentation pour de nombreux projets. Il est basé sur Markdown et prend en charge les extensions Markdown, les thèmes personnalisés et les plugins pour étendre ses capacités de rendu.

### Extrait du programme

!!! col _3
    **Contenu**

    Vocabulaire de la POO.
!!! col _3
    **Capacité**

    Ecrire la définition d'une classe.
!!! col _3
    **Commentaires**

    L'héritage et le polymorphisme ne sont pas au programme.

!!! col _2
    Colonne 1 / 2
!!! col _2
    Colonne 2 / 2

!!! col _3
    Colonne 1 / 3
!!! col _3
    Colonne 2 / 3
!!! col _3
    Colonne 3 / 3

### Plugins intéressants pour Mkdocs

- minify:
    - pour minimiser la taille des fichiers html, css, js
    - installation du plugin : python -m pip install mkdocs-minify-plugin

- exclude:
    - pour exclure des fichiers du build
    - installation du plugin : python -m pip install mkdocs-exclude

- awesome-pages:  
    - installation du plugin : python -m pip install mkdocs-awesome-pages-plugin
    - dans `mkdocs.yml`, inscrire l'option: `#!yaml collapse_single_pages: false` et ne pas mettre `#!yaml nav: ...` 
    - créer des fichiers `.pages` dans chaque dossier à l'intérieur de `docs`, par ex. pour le chapitre 1 : 

        ```txt title=".pages"
        title : chap 1 
        nav :
            - Cours: cours.md
            - Exercices: exercices.md
        ```

- mkdocs-autorefs: permet de gérer les liens internes facilement même avec une grande "profondeur" de dossiers, [https://github.com/mkdocstrings/autorefs/](https://github.com/mkdocstrings/autorefs/)
- mkdocs-jupyter
- mkdocs-inclu-exclude-files
- jupyterlite
- glightbox : pour afficher des images et zoomer facilement
- macros (pour pyodide ?)
- page-to-pdf ou mkdocs-with-pdf

### Insérer du code

Les codes sont colorés.

**Insérer du code en ligne**

Quand InlineHilite est activé, on peut appliquer une coloration syntaxique à un code en ligne en préfixant avec un shebang, càd #!, directement suivi par le nom du langage adéquat puis du code en ligne.

Exemple  avec  `` `#!python range()` `` qui donne :

The `#!python range()` function is used to generate a sequence of numbers.

The range() function is used to generate a sequence of numbers.


Entrée :
```` 
```python linenums="1"
def ma_fonction(param):
   y = param*6 + 5
   return y
```
````

Sortie :
```python linenums="1"
def ma_fonction(param):
   y = param*6 + 5
   return y
```

Entrée :
```` 
```python hl_lines="2"
def ma_fonction(param):
   y = param*6 + 5
   return y
```
````

Sortie :
```python hl_lines="2"
def ma_fonction(param):
   y = param*6 + 5
   return y
```

num de lignes + surlignage

Entrée :
```` 
```python linenums="1" hl_lines="2 3"
def ma_fonction(param):
   y = param*6 + 5
   return y
```
````

Sortie :
```python linenums="1" hl_lines="2 3"
def ma_fonction(param):
   y = param*6 + 5
   return y
```

#### Remarque sur les arguments

On peut passer des arguments à une fonction sous deux formes :

- sans mot clef (ou positionnels) (non-keyword arguments ou positional arguments) : ils sont ordonnés, comme dans une liste ;
- avec mot clef (keyword arg = kwarg) : ils possèdent une clef (un nom), comme dans un dictionnaire, et ne sont pas ordonnés.

```python hl_lines="1"
def ma_fonction(arg1, arg2, kwarg1 = "e", kwarg2 = 5):
   print(arg1, arg2, karg1, karg2)
```

Dans cet exemple, arg1  et arg2  sont des arguments positionnels, sans mot clef : ils sont obligatoires !

Alors que kwarg1  et kwarg2  sont des arguments avec mot clef : ils sont facultatifs, possèdent une valeur par défaut, et on peut les faire passer dans le désordre (mais obligatoirement après tous les arguments positionnels !)

<!-- marche pas pour l'instant sur le site en ligne
### Inclure du code d'un autre fichier

Affichage de `docs/scripts/test.py` # ATTENTION pas d'espace après "test.py"

Entrée :

```` 
```python title="test.py"
--8<-- "docs/scripts/test.py" 
``` 
````

Sortie :

```python title="test.py"
--8<-- "docs/scripts/test.py"
```

Entrée 2: avec surlignement de la ligne 1

````
```python hl_lines="1" title="test.py"
--8<-- "docs/scripts/test.py" 
```
````

Sortie 2: avec surlignement de la ligne 1

```python hl_lines="1" title="test.py"
--8<-- "docs/scripts/test.py"
```
-->
### Insérer des annotations dans le code

#### pour un seul fichier

Entrée : utilisation de `annotate` dans l'en-tête et de `# (1)` dans le code

``````
``` { .python .annotate title="test.py"}
# pour un seul fichier
# theme:
#  features:
#      pas de ceci: - content.code.annotate 
a = 2 # (1)
a = 3 # (2)
```

1. c'est une affectation, on affecte la valeur `2` à la variable `a`, `a` prend la valeur `2`
2. `a` prend la valeur `3` et non `5`
``````

Sortie :

``` { .python .annotate title="test.py"}
# pour un seul fichier
# theme:
#  features:
#      pas de ceci: - content.code.annotate 
a = 2 # (1)
a = 3 # (2)
```

1. c'est une affectation, on affecte la valeur `2` à la variable `a`, `a` prend la valeur `2`
2. `a` prend la valeur `3` et non `5`

#### pour tous les fichiers

``` yaml  title="mkdocs.yml"   
# pour tous les fichiers, mettre les 3 lignes suivantes dans mkdocs.yml
theme:
  features:
    - content.code.annotate # (1)
```

1. I'm a code annotation! I can contain `code`, __formatted
text__, images, ... basically anything that can be written in Markdown.

### Insérer les symboles des touches du clavier

Pour plus d'informations sur la liste des touches, voir le site de [facelessuser](https://facelessuser.github.io/pymdown-extensions/extensions/keys/#extendingmodifying-key-map-index)

Entrée :

```++ctrl+alt+delete++```

Sortie :

++ctrl+alt+delete++

### Insérer des maths à la façon LateX

Entrée :

```Soit $x\in\mathbb{R}$ tel que $x<2$ alors on a :```

```$$-sqrt({2}) < x < sqrt({2})$$```

Sortie :

Soit $x\in\mathbb{R}$ tel que $x<2$ alors on a :

$$-sqrt({2}) < x < sqrt({2})$$

### Tableaux en Markdown

[Site aidant à faire des tableaux, notamment en Markdown](https://www.tablesgenerator.com/markdown_tables)

## Alcasar

ALCASAR est un contrôleur sécurisé d'accès à Internet pour les réseaux publics, domestiques ou d'entreprise. Il authentifie et protège les connexions des utilisateurs indépendamment de leurs équipements (PC, tablette, smartphone, console de jeux, TV, etc.).

Il trace et impute les connexions conformément aux lois françaises et européennes. Il intègre plusieurs mécanismes de filtrage adaptés aux besoins privés (contrôle parental et filtrage des objets connectés) ou aux chartes et règlements internes (entreprises, lycées, associations, centres de loisirs et d'hébergement, bibliothèques, mairies, etc.).

## Qu'est-ce que Docker ?

Les programmeurs sont confrontés à un problème lorsque quelque chose fonctionne sur leur ordinateur portable mais nulle part ailleurs ? Auparavant, les développeurs créaient un programme qui isolait une partie des ressources physiques de l'ordinateur (mémoire, disque dur, etc.) et les transformait en une machine virtuelle.

Une machine virtuelle prétend être un ordinateur complet à elle seule. Elle possède même son propre système d'exploitation. Sur ce système d'exploitation, on déploie une application ou installe une bibliothèque et on la teste.

Les machines virtuelles consomment beaucoup de ressources, ce qui a suscité l'invention des conteneurs. L'idée est analogue à celle des conteneurs d'expédition. Avant l'invention des conteneurs d'expédition, les fabricants devaient expédier des marchandises dans une grande variété de tailles, d'emballages et de modes (camions, trains, bateaux).

En standardisant le conteneur d'expédition, ces marchandises pouvaient être transférées entre différents modes d'expédition sans aucune modification. La même idée s'applique aux conteneurs de logiciels.

Les conteneurs sont une unité légère de code et ses dépendances d'exécution, emballés de manière standardisée, afin qu'ils puissent être rapidement branchés et exécutés sur le système d'exploitation Linux. On n'a pas besoin de créer un système d'exploitation virtuel complet, comme on le ferai avec une machine virtuelle.

Les conteneurs ne reproduisent que les parties du système d'exploitation dont ils ont besoin pour fonctionner. Cela réduit leur taille et leur permet d'améliorer considérablement leurs performances.

Docker est actuellement la principale plateforme de conteneurs, et elle est même capable d'exécuter des conteneurs Linux sur Windows et macOS. Pour créer un conteneur Docker, on a besoin d'une image Docker. Les images fournissent des plans pour les conteneurs, tout comme les classes fournissent des plans pour les objets. 

## Qu'est-ce que les outils d'intégration continue ?

L'intégration continue (en anglais Continuous Integration (CI)) est la pratique consistant à construire et à tester fréquemment chaque modification apportée à une base de code. L'intégration continue implique que les développeurs téléchargent un nouveau code, ou des modifications de code, dans un référentiel de code commun, qui est ensuite testé automatiquement au moment du téléchargement pour s'assurer que les modifications ne causent pas de problèmes ou de ruptures. En automatisant les tests, les utilisateurs s'assurent immédiatement que leur code est solide et que les fonctionnalités importantes du logiciel fonctionnent comme prévu. En consolidant le code dans une source unifiée et en testant les failles du code dès le début d'un projet ou d'un sprint, les équipes de développement peuvent gagner du temps et éviter les retards dans la livraison de nouveaux logiciels, applications et fonctionnalités.

Les systèmes d'intégration continue font partie intégrante du flux de travail de DevOps et sont définis de manière unique par chaque entreprise. Ils fonctionnent souvent de manière transparente aux côtés de logiciels de gestion de la configuration, de logiciels de livraison continue et de logiciels de déploiement continu.

Pour être inclu dans la catégorie intégration continue, un produit doit :

- permettre aux développeurs de consolider le code dans un référentiel de code partagé ;
- effectuer des tests automatisés sur le code nouvellement écrit ;
- afficher une liste des tests réussis et échoués;
- effectuer toutes les actions nécessaires pour créer une version entièrement fonctionnelle du logiciel lorsque tous les tests sont réussis.

## Raccourcis clavier sur VS Code

### Raccourcis généraux

- Ctrl + Maj + P, F1 : Ouvrir la palette
- Ctrl + Maj + N : nouvelle instance de VSCode
- Ctrl + , : Paramètres de l'utilisateur
- Ctrl + K  Ctrl + S : Liste des raccourcis clavier

### Raccourcis d'édition de base

- Ctrl + X : Couper une ligne
- Ctrl + C : Copier une ligne
- Ctrl + V : Coller une ligne
- Alt + Haut ou Bas : Déplacer une ligne en haut ou en bas
- Ctrl + Maj + Alt + Haut ou Bas : Copier une ligne en haut ou en bas
- Ctrl + Maj + K : Supprimer une ligne
- Enter : Insérer une ligne en bas
- Ctrl + Maj + Enter : Insérer une ligne en haut
- Ctrl + Maj + :    : Activer/désactiver le commentaire de ligne ou de bloc
- Home / End : Go to beginning/end of line
- Ctrl + Home End : Go to beginning/end of file
- Alt + Z : Activer retour à la ligne en fonction de la largeur de fenêtre
- Tab : Indentation (ligne ou bloc)
- Maj + Tab : Indentation à l'arrière (ligne ou bloc)

### Multi-curseurs et sélection

- Alt + Clic Insérer un curseur
- Ctrl+W Close editor
- Shift+Alt+ ↑ / ↓ Insert cursor above/below
- Ctrl+K F Close folder
- Ctrl+ Undo last cursor operation
- Ctrl+\ Split editor
- Shift+Alt+I Insert cursor at end of each line selected
- Ctrl+ 1 / 2 / 3 Focus into 1st, 2nd, 3rd editor group
- Ctrl+L Select current line
- Ctrl+K Ctrl + ← Focus into previous editor group
- Ctrl+Shift+L Select all occurrences of current selection
- Ctrl+K -  Ctrl + → Focus into next editor group
- Ctrl+F2 Select all occurrences of current word
- Ctrl+Shift+PgUp Move editor left
- Shift+Alt + → Expand selection
- Ctrl+Shift+PgDn Move editor right
- Shift+Alt + ← Shrink selection
- Ctrl+K ← Move active editor group left/up
- Shift+Alt + drag mouse Column (box) selection

### Raccourcis VS Code pour la recherche et le remplacement

- Ctrl + F : Rechercher
- Ctrl + H : Remplacer
- F3 / Maj + F3 : Trouver l'occurence suivante / précédente
- Alt + Entrée : Sélectionner toutes les occurences de l'élément recherché

### Raccourcis VS Code pour la navigation

- Ctrl+G Go to Line
- Ctrl + P choisir un fichier
- Ctrl+Shift+ O : Aller à un symbole
- F12 Go to Definition
- Ctrl+P Go to File
- Shift+ PgUp / PgDn Scroll page up/down
- Ctrl+Shift+F10 Peek Definition
- Ctrl+Shift+O Go to Symbol
- Ctrl + Maj + M : Ouvrir le panneau de problèmes

### Raccourcis VS Code pour la gestion des fichiers

- Ctrl + N : Créer un nouveau fichier
- Ctrl + O : Ouvrir un fichier
- Ctrl + S : Enregistrer
- Ctrl + Maj + S : Enregistrer sous...
- Ctrl + Maj + T : Ouvrir de nouveau un fichier fermé
- Ctrl + Tab : Ouvrir le fichier suivant
- Ctrl + Maj + Tab : Ouvrir le fichier précédent

### Raccourcis VS Code pour l'affichage

- F11 : plein écran
- Ctrl + + : Zoom avant
- Ctrl + - : Zoom arrière
- Ctrl + B : Activer/désactiver le sidebar
- Shift + Alt + 0 : Activer/désactiver la présentation des fenêtres (horizontal/vertical)
- Ctrl + Maj + C : Ouvrir la fenêtre d'un terminal pointant sur le dossier actuel
- Ctrl + K  Ctrl + H : Ouvrir le panneau de sortie
- Ctrl + K  V : ouvrir sur un panneau latéral l'aperçu d'un fichier Markdddown

## Environnement virtuel

[venv : documentation officelle de Python](https://docs.python.org/fr/3.6/tutorial/venv.html)

[virtualenv, issu de venv mais plus rapide avec plus de fonctionnalités](https://realpython.com/python-virtual-environments-a-primer/)

### sous Linux avec venv (builtin Python)

``` bash
$ mkdir mkdocs-documentation
$ cd mkdocs-documentation
$ python3 -m venv venv
$ source venv/bin/activate
(venv) $ python -m pip install mkdocs
(venv) $ python -m pip install "mkdocstrings[python]"
(venv) $ python -m pip install mkdocs-material

# pip list liste tous les paquets installés dans l’environnement virtuel
(venv) $ python -m pip list
# pip freeze produit une liste similaire des paquets installés 
#mais l’affichage adopte un format que pip install peut lire. 
# La convention habituelle est de mettre cette liste dans un fichier requirements.txt :
(venv) $ python -m pip freeze > requirements.txt
# Le fichier requirements.txt peut alors être ajouté dans un système 
# de gestion de versions (git par ex.) comme faisant partie de votre application.
# Les utilisateurs peuvent alors installer tous les paquets 
# nécessaires à l’application avec install -r :
$ python3 -m venv new-venv
$ source new-venv/bin/activate
(new-venv) $ python -m pip install -r requirements.txt

# Désactiver `venv`
(venv) $ deactivate
# Réactiver `venv`
$ source venv/bin/activate

# Quelques commandes Git
(venv) $ git init
(venv) $ git remote add origin urlrepo
(venv) $ git add .
(venv) $ git commit -m "Add project code and documentation"
(venv) $ git push origin main

# Passer de la branche locale master à main, comme sur Gitlab
(venv) $ git branch -m master main
```

### sous Windows PS PowerShell avec venv (builtin Python)

``` shell
PS> mkdir mkdocs-documentation
PS> cd mkdocs-documentation
PS> python -m venv venv
PS> venv\Scripts\activate
(venv) PS> python -m pip install mkdocs
(venv) PS> python -m pip install "mkdocstrings[python]"
(venv) PS> python -m pip install mkdocs-material
```

### sous Linux avec virtualenv (à installer)

``` sh
# Installer virtualenv
$ pip install virtualenv

# Acceder au dossier du projet
$ cd my_folder

# Créer l'environnement virtuel  `venv`
$ virtualenv venv

# Indiquer l'interpréteur (3.7) à utiliser pour notre environnement 'venv'
$ virtualenv -p /usr/bin/python3.7 venv

# Activer `venv`
$ source venv/bin/activate

# Désactiver `venv`
$ deactivate

# Générer les bibliothéques utilisées
$ venv/bin/pip freeze > requirements.txt

# Installer les bibliothèques automatiquement depuis le fichier 'requirements.txt' 
$ venv2/bin/pip install -r requirements.txt
```

### sous Windows avec virtualenv (à installer)

``` shell
# Installer virtualenv
$ pip install virtualenv

# Acceder au dossier du projet
$ cd my_folder

# Créer l'environnement virtuel  `venv`
$ virtualenv venv

# Indiquer l'interpreteur (2.7) à utiliser pour notre environnement 'venv'
$ virtualenv -p /usr/bin/python2.7 venv

# Activer `venv`
$ venv\Scripts\activate

# Désactiver `venv`
$ venv\Scripts\deactivate

# Générer les bibliothéques utilisées
$ venv\Scripts\pip freeze > requirements.txt

# Installer les bibliothèques automatiquement depuis le fichier 'requirements.txt'
$ venv\Scripts\pip install -r requirements.txt
```

## Langages et programmation

Un programme informatique est un ensemble d’opérations destinées à être exécutées par un ordinateur.

Un logiciel est un ensemble de composants numériques destiné à fournir un service informatique; un logiciel peut comporter plusieurs programmes.

Il existe de nombreux langages de programmation qui permettent de programmer un ordinateur,  différents par leur style (impératif, fonctionnel, objet, événementiel, etc.).

Le seul langage directement utilisable par le processeur est le langage machine (voir architecture de Von Neumann), et chaque famille de processeurs possède son propre langage.

Les informaticiens préfèrent, dans la très grande majorité des cas, utiliser des langages de haut niveau qui sont plus simples à utiliser, car plus proches du langage naturel (exemple : si a=3 alors b=c).

Exemples de langages de haut niveau : C, C++ , Java, Python, …

Un programme spécialisé assure la traduction vers le langage machine. Si la traduction est faite en une seule opération, il s’agit d’un **compilateur**; si au contraire la traduction est réalisée au fur et à mesure du déroulement du programme, on parlera d’**interpréteur**.

[Inspection en profondeur de la compilation et de l’interprétation](https://dev.to/vaidehijoshi/a-deeper-inspection-into-compilation-and-interpretation-8bp)

    Remarque : il existe des langages formalisés de description (HTML, …) ou de requêtes (SQL, …) : ce ne sont pas des langages de programmation.

Le saviez-vous ?

Le premier programmeur de l’histoire était … une programmeuse !

Ada Lovelace est une pionnière de la science informatique. Elle est principalement connue pour avoir réalisé le premier programme informatique, lors de son travail sur un ancêtre de l’ordinateur : la machine analytique de Charles Babbage.
