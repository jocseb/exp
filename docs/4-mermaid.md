# Mermaid

On peut directement inclure un diagramme 

!!! note "Entrée"
    ````markdown
    Un _joli_ diagramme

    ```mermaid
    graph TD;
        A-->B;
        A-->C;
        B-->D;
        C-->D;
    ```
    ````

!!! done "Rendu"
    Un _joli_ diagramme

    ```mermaid
    graph TD;
        A-->B;
        A-->C;
        B-->D;
        C-->D;
    ```


Pour de plus amples informations, on pourra consulter le site officiel : [Mermaid](https://mermaid-js.github.io/mermaid/#/)

```mermaid
        graph TD
        S15["15"] --> S6["6"]
        S15 --> S18["18"]
        S6 --> S3["3"]
        S6 --> S7["7"]
        S18 --> S17["17"]
        S18 --> S20["20"]
        S3 --> S2["2"]
        S3 --- V1[" "]
        style V1 fill:#FFFFFF, stroke:#FFFFFF
        linkStyle 7 stroke:#FFFFFF,stroke-width:0px
```

```mermaid
        graph TD
        S10["10"] --> S3["3"]
        S10 --> S12["12"]
        S3 --> S2["2"]
        S3 --> S5["5"]
        S12 --> S11["11"]
        S12 --> S13["13"]
```

1. Ce fonctionnement traduit le comportement d'une **file** c'est-à-dire que le premier élément qui entre dans la structure de données est aussi le premier à en sortir (*FIFO* pour *First In, First Out*). Dans une pile, la dernier élément entré est le premier à sortir (*LIFO* pour *Last In, First Out*)

2.  a. C'est la **taille** de l'arbre (c'est-à-dire le nombre total de noeuds de l'arbre)

    b. C'est la **racine** de l'arbre puisqu'il s'agit de la tâche ajoutée à l'arbre en premier

    c. C'est une **feuille** de l'arbre aucune tâche n'ayant été ajouté après celle-ci, le noeud représentant cette tâche n'a pas de fils, c'est donc une feuille.

3.  a. Les attributs de la classe `Noeud` sont `tache`, `indice`, `gauche` et `droite`.

    b. La méthode `insere` est récursive car elle s'appelle elle-même. Elle se termine car à chaque appel on descend d'un niveau dans l'arbre.

    c. On insère à gauche lorsque la valeur à insérer est inférieure à celle du noeud courant donc on complète la ligne 26 par :
    ```python
    elif self.racine.indice > nouveau_noeud.indice
    ```

    d. 
    
    * Arbre initial
    ```mermaid
        graph TD
        S12["12"] --> S6["6"]
        S12 --> S14["14"]
        S6 --- V1[" "]
        S6 --> S10["10"]
        S10 --> S8["8"]
        S10 --- V2[" "]
        S14 --> S13["13"]
        S14 --- V3[" "]
        style V1 fill:#FFFFFF, stroke:#FFFFFF
        style V2 fill:#FFFFFF, stroke:#FFFFFF
        style V3 fill:#FFFFFF, stroke:#FFFFFF
        linkStyle 2 stroke:#FFFFFF,stroke-width:0px
        linkStyle 5 stroke:#FFFFFF,stroke-width:0px
        linkStyle 7 stroke:#FFFFFF,stroke-width:0px
    ```

    * Après insertion du 11
    ```mermaid
        graph TD
        S12["12"] --> S6["6"]
        S12 --> S14["14"]
        S6 --- V1[" "]
        S6 --> S10["10"]
        S10 --> S8["8"]
        S10 --> S11["11"]
        S14 --> S13["13"]
        S14 --- V3[" "]
        style V1 fill:#FFFFFF, stroke:#FFFFFF
        style V3 fill:#FFFFFF, stroke:#FFFFFF
        linkStyle 2 stroke:#FFFFFF,stroke-width:0px
        linkStyle 7 stroke:#FFFFFF,stroke-width:0px
    ```

    * Après insertion du 5
    ```mermaid
        graph TD
        S12["12"] --> S6["6"]
        S12 --> S14["14"]
        S6 --> S5["5"]
        S6 --> S10["10"]
        S10 --> S8["8"]
        S10 --> S11["11"]
        S14 --> S13["13"]
        S14 --- V3[" "]
        style V3 fill:#FFFFFF, stroke:#FFFFFF
        linkStyle 7 stroke:#FFFFFF,stroke-width:0px
    ```

    * Après insertion du 16
    ```mermaid
        graph TD
        S12["12"] --> S6["6"]
        S12 --> S14["14"]
        S6 --> S5["5"]
        S6 --> S10["10"]
        S10 --> S8["8"]
        S10 --> S11["11"]
        S14 --> S13["13"]
        S14 --> S16["16"]
    ```

    * Après insertion du 7
    ```mermaid
        graph TD
        S12["12"] --> S6["6"]
        S12 --> S14["14"]
        S6 --> S5["5"]
        S6 --> S10["10"]
        S10 --> S8["8"]
        S10 --> S11["11"]
        S14 --> S13["13"]
        S14 --> S16["16"]
        S8 --> S7["7"]
        S8 --- V1[" "]
        style V1 fill:#FFFFFF, stroke:#FFFFFF
        linkStyle 9 stroke:#FFFFFF,stroke-width:0px
    ```
4. 
```python linenums='41'
def est_present(self,indice_recherche):
    """renvoie True si l'indice de priorité indice_recherche (int) passé en paramètre est déjà l'indice d'un noeud de l'abre, False sinon"""
    if self.racine == None:
        return False
    else:
        if self.racine.indice == indice_recherche:
            return True
        elif self.racine.indice > indice_recherche:
            return self.racine.gauche.est_present(indice_recherche)
        else:
            return self.racine.droite.est_present(indice_recherche)
```

5.  a. On rappelle que dans un parcours *infixe*, on parcourt le sous arbre gauche, puis la racine, puis le sous arbre droit. Dans le cas de l'arbre de la figure 1, on obtient : ["6","8","10","12","13","14"]

    b. La tâche la plus prioritaire sera le premier élément rencontré lors de ce parcours.

6. 
```python linenums="1"
def tache_prioritaire(self):
    """renvoie la tache du noeud situé le plus à gauche de l'ABR supposé non vide"""
    if self.racine.gauche.est_vide():
        return self.racine.tache
    else:
        return self.racine.gauche.tache_prioritaire()
```

7.  

    * Etape 1 : tâche d'indice 14 à accomplir
    ```mermaid
        graph TD
        S14["14"]
    ```

    * Etape 2 : tâche d'indice 11 à accomplir
    ```mermaid
        graph TD
        S14["14"] --> S11["11"]
        S14 --- V1[" "]
        style V1 fill:#FFFFFF, stroke:#FFFFFF
        linkStyle 1 stroke:#FFFFFF,stroke-width:0px
    ```

    * Etape 3 : tâche d'indice 8 à accomplir
    ```mermaid
        graph TD
        S14["14"] --> S11["11"]
        S14 --- V1[" "]
        S11 --> S8["8"]
        S11 --- V2[" "]
        style V1 fill:#FFFFFF, stroke:#FFFFFF
        style V2 fill:#FFFFFF, stroke:#FFFFFF
        linkStyle 1 stroke:#FFFFFF,stroke-width:0px
        linkStyle 3 stroke:#FFFFFF,stroke-width:0px
    ```

    * Etape 4 : accomplir la tâche prioritaire (8) 
    ```mermaid
        graph TD
        S14["14"] --> S11["11"]
        S14 --- V1[" "]
        style V1 fill:#FFFFFF, stroke:#FFFFFF
        linkStyle 1 stroke:#FFFFFF,stroke-width:0px
    ```

    * Etape 5 : tâche d'indice 12 à accomplir
    ```mermaid
        graph TD
        S14["14"] --> S11["11"]
        S14 --- V1[" "]
        S11 --- V2[" "]
        S11 --> S12["12"]
        style V1 fill:#FFFFFF, stroke:#FFFFFF
        style V2 fill:#FFFFFF, stroke:#FFFFFF
        linkStyle 1 stroke:#FFFFFF,stroke-width:0px
        linkStyle 2 stroke:#FFFFFF,stroke-width:0px
    ```

    * Etape 6 : accomplir la tâche prioritaire (11) 
    ```mermaid
        graph TD
        S14["14"] --> S12["12"]
        S14 --- V1[" "]
        style V1 fill:#FFFFFF, stroke:#FFFFFF
        linkStyle 1 stroke:#FFFFFF,stroke-width:0px
    ```

    * Etape 7 : accomplir la tâche prioritaire (12) 
    ```mermaid
        graph TD
        S14["14"]
    ```

    * Etape 8 : tâche d'indice 15 à accomplir
    ```mermaid
        graph TD
        S14["14"] --- V1[" "]
        S14 --> S15["15"]
        style V1 fill:#FFFFFF, stroke:#FFFFFF
        linkStyle 0 stroke:#FFFFFF,stroke-width:0px
    ```

    * Etape 9 : tâche d'indice 19 à accomplir
    ```mermaid
        graph TD
        S14["14"] --- V1[" "]
        S14 --> S15["15"]
        S15 --- V2[" "]
        S15 --> S19["19"]
        style V1 fill:#FFFFFF, stroke:#FFFFFF
        linkStyle 0 stroke:#FFFFFF,stroke-width:0px
        style V2 fill:#FFFFFF, stroke:#FFFFFF
        linkStyle 2 stroke:#FFFFFF,stroke-width:0px
    ```

    * Etape 10 : accomplir la tâche prioritaire (14)
    ```mermaid
        graph TD
        S15["15"] --- V1[" "]
        S15 --> S19["19"]
        style V1 fill:#FFFFFF, stroke:#FFFFFF
        linkStyle 0 stroke:#FFFFFF,stroke-width:0px
    ```